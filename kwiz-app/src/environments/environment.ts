// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDqXEIv8QJXO0Rp4mWlQvA70l5vIXANpjc',
    authDomain: 'kwiz-ad296.firebaseapp.com',
    projectId: 'kwiz-ad296',
    storageBucket: 'kwiz-ad296.appspot.com',
    messagingSenderId: '618264767624',
    appId: '1:618264767624:web:915d86413cda543543433d',
    measurementId: 'G-9PGCS26GY6',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
