import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionSequence } from 'selenium-webdriver';
import { AuthService } from '../auth/auth.service';
import { QuizService } from '../quiz.service';
import { AttemptedQuiz } from '../_models/attempted-quiz';
import { Question } from '../_models/question';
import { Quiz } from '../_models/quiz'; 
import { User } from '../_models/user';

@Component({
  selector: 'app-user-quiz-history',
  templateUrl: './user-quiz-history.component.html',
  styleUrls: ['./user-quiz-history.component.css']
})
export class UserQuizHistoryComponent implements OnInit {
  questions: any[]=[];
  ansList:number[]=[];
  opt:number[]=[0,1,2,3];
  Qlevel:string="";
  quizCat:string="";
  quizName:string="";
  Qnumber:number=0;
  quizIdHex:string="";
  username:any;
  score:number=0;
  quiz:Quiz | undefined;
  user:User | undefined;
  date:Date | undefined;
 
  constructor(private auth:AuthService,private quizservice:QuizService,private route:Router,private activeroute:ActivatedRoute) { 
    
    this.activeroute.params.subscribe((newparams)=>{
      this.quizCat=newparams['category'],
      this.quizIdHex=newparams['quizId'],
      this.date=newparams['date']
     
    })
    this.quizservice.getQuizById(this.quizCat,this.quizIdHex).subscribe((querySnapshot) => {
          this.quiz=querySnapshot.data() as Quiz;
          this.questions=this.quiz.questions;
          this.quizName=this.quiz.name;
          if(this.quiz.level==0) {this.Qlevel="Easy"}
          else if(this.quiz.level==1) {this.Qlevel="Medium"}
          else {this.Qlevel="Difficult"}
        });
        
      this.auth.getUser().subscribe((userDetails) => {
        if (userDetails) this.username = userDetails.displayName || '';
      });
      this.auth.getAttemptedQuizes().subscribe((user)=>{
        this.user=user.payload.data() as User;
        console.log(user.payload.data());
        for(let a of this.user.attemptedQuizes)
        {
          if(a.quizId==this.quizIdHex)
          {
            this.ansList=a.answers;
            this.score=a.score;
          }
        }    
      })
     
     
  }

  ngOnInit(): void {
   
  }
  Incre():number{
    this.Qnumber=this.Qnumber+1;
    return this.Qnumber;
  }
  setZero(){
    this.Qnumber=0;
  }

  home():void{
  this.route.navigateByUrl("home");
  }

}
