import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuizService } from '../quiz.service';

@Component({
  selector: 'app-history-quiz',
  templateUrl: './history-quiz.component.html',
  styleUrls: ['./history-quiz.component.css'],
})
export class HistoryQuizComponent implements OnInit {
  entry: any;
  params: any; // used when direct access
  quiz_level = ['EASY', 'MEDIUM', 'HARD'];
  constructor(
    private quizService: QuizService,
    private activatedRoute: ActivatedRoute,
    private route: Router
  ) {
    this.activatedRoute.params.subscribe((params) => {
      this.params = params;
    });
  }

  ngOnInit(): void {
    this.entry = this.quizService.getHistoryEntry();
    console.log(this.entry);
    if (!this.entry) {
      // TODO
      // for not redirect to profile
      this.route.navigateByUrl('/profile');
    }
  }
}
