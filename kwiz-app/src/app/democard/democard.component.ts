import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuizService } from '../quiz.service';
import { AttemptedQuiz } from '../_models/attempted-quiz';
import { Quiz } from '../_models/quiz';

@Component({
  selector: 'app-democard',
  templateUrl: './democard.component.html',
  styleUrls: ['./democard.component.css'],
})
export class DemocardComponent implements OnInit {
  params: {
    quizId: string;
    category: string;
  } | null = null;

  quiz: Quiz | null = null;
  answerlist: number[] = [];
  visited: boolean[] = [];

  quiz_level = ['EASY', 'MEDIUM', 'HARD'];

  currentIndex = 0;

  constructor(
    private router: Router,
    private activatedroute: ActivatedRoute,
    private quizservice: QuizService
  ) {}
  ngOnInit(): void {
    this.activatedroute.queryParamMap.subscribe((params) => {
      this.params = {
        quizId: params.get('quizId') || '',
        category: params.get('category') || '',
      };
      console.log(this.params);
      this.fetchQuestions();
    });
  }

  addQuiz() {}

  fetchQuestions() {
    if (!this.params) throw new Error('Params empty');

    this.quizservice
      .getQuizById(this.params.category, this.params.quizId)
      .subscribe((doc) => {
        console.log('Fetching... quizes');
        console.log('fetched quiz', doc.data());
        this.quiz = doc.data() as Quiz;
        this.answerlist = new Array<number>(this.quiz.questions.length);
        this.visited = new Array<boolean>(this.quiz.questions.length);
      });
  }

  previousQuestion() {
    if (this.currentIndex != 0) this.currentIndex--;
  }
  nextQuestion() {
    if (this.currentIndex < this.quiz!.questions.length) this.currentIndex++;
  }

  submitTest() {
    let score = this.calculateScore();
    this.quizservice.addQuizResult(
      new AttemptedQuiz(
        this.params!.quizId,
        this.params!.category,
        this.answerlist,
        score,
        new Date()
      )
    );
    this.quizservice.setResult(score);
    this.router.navigateByUrl('/result');
  }

  calculateScore(): number {
    let score = 0;
    for (let i = 0; i < this.quiz!.questions.length; i++) {
      // question which not attempted
      // mark using -1 bcoz valid options [0-3]
      //if (!this.visited[i]) this.answerlist[i] = -1;

      if (
        this.visited[i] &&
        this.answerlist[i] == this.quiz!.questions[i].answerIndex
      ) {
        score++;
      }
    }
    return score;
  }

  markAnswer(option: number) {
    console.log('marking option ', option);
    this.visited[this.currentIndex] = true;
    this.answerlist[this.currentIndex] = option;
  }

  wasSelected(option: number) {
    return (
      this.visited[this.currentIndex] == true &&
      this.answerlist[this.currentIndex] == option
    );
  }
}
